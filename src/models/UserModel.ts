import { mapTo } from '@wufe/mapper'
import { Table, Model, Default, DataType, PrimaryKey, Column, BelongsToMany, HasMany } from 'sequelize-typescript'
import { ChannelModel } from './ChannelModel'
import { ChannelUserModel } from './ChannelUserModel'
import { MessageModel } from '.'
import { UserDto } from '@dtos'

@mapTo(UserDto)
@Table({
    tableName: 'user'
})
export class UserModel extends Model<UserModel> {

    @Default(DataType.UUIDV4)
    @PrimaryKey
    @Column(DataType.UUID)
    public id!: string

    @BelongsToMany(() => ChannelModel, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        through: () => ChannelUserModel
    })
    public channels?: ChannelModel[]

    @HasMany(() => MessageModel) 
    public messages?: MessageModel[]



}