import { Table, Model, ForeignKey, Column, DataType, AllowNull } from 'sequelize-typescript'
import { ChannelModel, UserModel } from '@models'

@Table({
    tableName: 'user_channel'
})
export class ChannelUserModel extends Model<ChannelUserModel> {

    @ForeignKey(() => ChannelModel)
    @AllowNull(false)
    @Column({
        type: DataType.UUID
    })
    public channelID!: string

    @ForeignKey(() => UserModel)
    @AllowNull(false)
    @Column({
        type: DataType.UUID
    })
    public userID!: string

}