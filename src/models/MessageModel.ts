import { Table, Column, Model, PrimaryKey, Length, Max, ForeignKey, DataType, AllowNull, Default } from 'sequelize-typescript'
import { mapTo } from '@wufe/mapper'
import { MessageDto } from '@dtos'
import { UserModel, ChannelModel } from '@models'

@mapTo(MessageDto)
@Table({
    tableName: 'message'
})
export class MessageModel extends Model<MessageModel> {

    @Default(DataType.UUIDV4)
    @PrimaryKey
    @Column(DataType.UUID)
    public id!: string

    @ForeignKey(() => UserModel)
    @AllowNull(false)
    @Column({
        type: DataType.UUID
    })
    public userID!: string

    @ForeignKey(() => ChannelModel)
    @Column({
        type: DataType.UUID
    })
    public channelID!: string

    @Length({
        max: 255
    })
    @Column
    public text!: string

}