import { mapTo } from '@wufe/mapper'
import { Table, Model, PrimaryKey, Column, HasMany, Default, DataType, BelongsTo, BelongsToMany } from 'sequelize-typescript'
import { UserModel } from './UserModel'
import { ChannelUserModel } from './ChannelUserModel'
import { ChannelDto } from '@dtos'
import { MessageModel } from '@models'

@mapTo(ChannelDto)
@Table({
    tableName: 'channel'
})
export class ChannelModel extends Model<ChannelModel>{

    @Default(DataType.UUIDV4)
    @PrimaryKey
    @Column(DataType.UUID)
    public id!: string

    @HasMany(() => MessageModel) 
    public messages?: MessageModel[]

    @BelongsToMany(() => UserModel, {
        onDelete: 'CASCADE',
        through: () => ChannelUserModel
    })
    public users?: UserModel[]
}