import { ChannelDto, MessageDto } from '@dtos'
import { ApiModel, ApiModelProperty, SwaggerDefinitionConstant } from 'swagger-express-ts'

@ApiModel({
    name: 'User'
})
export class UserDto {

    @ApiModelProperty({
        description: 'Identification Attribute.'
    })
    public id: string

    @ApiModelProperty({
        description: 'Channels Objects.'
    })
    public channels: ChannelDto[]

    @ApiModelProperty({
        description: 'Messages Objects'
    })
    public messages: MessageDto[]

    @ApiModelProperty({
        description: 'Data created date.',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public createdAt?: Date;

    @ApiModelProperty({
        description: 'Data updated date.',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public updatedAt?: Date;

    constructor(id: string, channels: ChannelDto[], messages: MessageDto[]) {
        this.id = id
        this.channels = channels
        this.messages = messages
    }

}