import { ApiModel, ApiModelProperty, SwaggerDefinitionConstant } from 'swagger-express-ts'

@ApiModel({
    name: 'User'
})
export class MessageDto {

    @ApiModelProperty({
        description: 'Identification Attribute.',
    })
    public id: string

    @ApiModelProperty({
        description: 'User Id String.',
    })
    public userID: string

    @ApiModelProperty({
        description: 'Message content.',
    })
    public text: string

    @ApiModelProperty({
        description: 'channel Attribute.',
    })
    public channelID: string

    @ApiModelProperty({
        description: 'Created date of post',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public createdAt?: Date;

    @ApiModelProperty({
        description: 'Updated date of post',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public updatedAt?: Date;

    constructor(id: string, userID: string, text: string, channelID: string) {
        this.id = id
        this.userID = userID
        this.text = text
        this.channelID = channelID
    }
}