import { UserDto, MessageDto } from '@dtos'
import { ApiModel, ApiModelProperty, SwaggerDefinitionConstant } from 'swagger-express-ts'

@ApiModel({
    name: 'Channel'
})
export class ChannelDto {

    @ApiModelProperty({
        description: 'Identification Attribute.',
    })
    public id: string

    @ApiModelProperty({
        description: 'User object.',
    })
    public users: UserDto[]

    @ApiModelProperty({
        description: 'Messages Objects.',
    })
    public messages: MessageDto[]

    @ApiModelProperty({
        description: 'Created date of post',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public createdAt?: Date;

    @ApiModelProperty({
        description: 'Updated date of post',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public updatedAt?: Date;

    constructor(id: string, users: UserDto[], messages: MessageDto[]) {
        this.id = id
        this.users = users
        this.messages = messages
    }
}