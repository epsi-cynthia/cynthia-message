import * as express from 'express'

import { OK } from 'http-status-codes'
import { UserService, ChannelService, Helper } from '@services'
import { UserBody } from '@requestbody'
import { controller, interfaces, httpGet, response, requestParam, httpPost, request, httpDelete } from 'inversify-express-utils'
import { inject } from 'inversify'
import { KeycloakAuth } from '@shared'
import { ApiPath, ApiOperationGet, SwaggerDefinitionConstant, ApiOperationPost, ApiOperationDelete } from 'swagger-express-ts'

@ApiPath({
    path: '/users',
    name: 'User',
})
@controller('/users', KeycloakAuth.getInstance().protect())
export class UserController implements interfaces.Controller {

    constructor(
        @inject(ChannelService.name) private userService: UserService,
        @inject(Helper.name) private helper: Helper,
    ) { }

    @ApiOperationGet({
        summary: 'Get all users.',
        description: 'Get all users of database.',
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/')
    getAll(req: express.Request, res: express.Response) {
        return this.userService.getAll().then(users => {
            res.status(OK).json(users)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationGet({
        path: '/{id}',
        summary: 'Get unique user from id.',
        description: 'Get unique user from id given in params.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/:id')
    get(@requestParam("id") id: string, req: express.Request, res: express.Response) {
        return this.userService.get(id).then(user => {
            res.status(OK).json(user)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationPost({
        summary: 'Create a new user.',
        description: 'Create a new user in database.',
        parameters: {
            body: {
                type: SwaggerDefinitionConstant.OBJECT,
                required: true,
                model: 'UserBody',
            },
        },
        responses: {
            201: { description: 'Created' },
            400: { description: 'Parameters fail' },
        },
    })
    @httpPost('/create')
    create(req: express.Request, res: express.Response) {
        const user = new UserBody(req.body.id)
        return this.userService.create(user).then(user => {
            res.status(OK).json(user)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationDelete({
        path: '/{id}',
        summary: 'Delete a user.',
        description: 'Delete a user in database.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpDelete('/delete/:id')
    delete(@requestParam("id") id: string, req: express.Request, res: express.Response) {
        return this.userService.delete(id).then(user => {
            res.status(OK).json(user)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

}