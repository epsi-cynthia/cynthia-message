import * as express from 'express'

import { MessageService, ChannelService, Helper } from '@services'
import { MessageBody } from '@requestbody'
import { response, interfaces, httpGet, controller, requestParam, httpPost, request, httpDelete } from 'inversify-express-utils'
import { BAD_REQUEST, OK } from 'http-status-codes'
import { inject } from 'inversify'
import { KeycloakAuth } from '@shared'
import { ApiPath, ApiOperationGet, SwaggerDefinitionConstant, ApiOperationPost, ApiOperationDelete } from 'swagger-express-ts'

@ApiPath({
    path: '/messages',
    name: 'Message',
})
@controller('/messages', KeycloakAuth.getInstance().protect())
export class MessageController implements interfaces.Controller {

    constructor(
        @inject(ChannelService.name) private messageService: MessageService,
        @inject(Helper.name) private helper: Helper,
    ) { }

    @ApiOperationGet({
        summary: 'Get all messages.',
        description: 'Get all messages of database.',
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/')
    getAll(req: express.Request, res: express.Response) {
        return this.messageService.getAll().then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationGet({
        path: '/{id}',
        summary: 'Get unique message from id.',
        description: 'Get unique message from id given in params.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/:id')
    get(@requestParam("id") id: string, req: express.Request, res: express.Response) {
        this.messageService.get(id).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationPost({
        summary: 'Create a new message.',
        description: 'Create a new message in database.',
        parameters: {
            body: {
                type: SwaggerDefinitionConstant.OBJECT,
                required: true,
                model: 'MessageBody',
            },
        },
        responses: {
            201: { description: 'Created' },
            400: { description: 'Parameters fail' },
        },
    })
    @httpPost('/create')
    create(req: express.Request, res: express.Response) {
        const message = new MessageBody(req.body.user_id, req.body.channel_id, req.body.text)

        this.messageService.create(message).then(message => {
            res.status(OK).json(message)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationDelete({
        path: '/{id}',
        summary: 'Delete a message.',
        description: 'Delete a message in database.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpDelete('/delete/:id')
    delete(@requestParam("id") id: string, req: express.Request, res: express.Response) {
        return this.messageService.delete(id).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

}