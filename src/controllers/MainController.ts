import * as express from 'express'

import { MainService, Helper } from '@services'
import { OK } from 'http-status-codes'
import { controller, interfaces, httpPost, request, response } from 'inversify-express-utils'
import { inject } from 'inversify'
import { KeycloakAuth } from '@shared'
import { ApiOperationPost, SwaggerDefinitionConstant, ApiPath } from 'swagger-express-ts'

@ApiPath({
    path: '/index',
    name: 'Index',
})
@controller('/index', KeycloakAuth.getInstance().protect())
export class MainController implements interfaces.Controller {

    constructor(
        @inject(MainService.name) private mainService: MainService,
        @inject(Helper.name) private helper: Helper,
    ) { }

    @ApiOperationPost({
        summary: 'Post a message between two users',
        description: 'Post a message between two users.',
        parameters: {
            body: {
                type: SwaggerDefinitionConstant.OBJECT,
                model: 'sender_id, holder_id, text',
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpPost('/postMessage')
    postMessage(req: express.Request, res: express.Response) {

        this.mainService.postMessage(req.body.sender_id, req.body.holder_id, req.body.text).then(message => {
            res.status(OK).json(message)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })

    }

}