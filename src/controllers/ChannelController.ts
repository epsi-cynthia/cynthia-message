import * as express from 'express'

import { ChannelService, Helper } from '@services'
import { BAD_REQUEST, OK } from 'http-status-codes'
import { ChannelUserBody } from '@requestbody'

import { interfaces, controller, httpGet, httpPost, httpDelete, request, queryParam, response, requestParam, httpPut } from "inversify-express-utils"
import { inject } from 'inversify'
import { KeycloakAuth } from '@shared'
import { ApiPath, ApiOperationGet, SwaggerDefinitionConstant, ApiOperationPost, ApiOperationDelete, ApiOperationPut } from 'swagger-express-ts'
import { body } from 'express-validator'

@ApiPath({
    path: '/channels',
    name: 'Channel',
})
@controller('/channels', KeycloakAuth.getInstance().protect())
export class ChannelController implements interfaces.Controller {

    constructor(
        @inject(ChannelService.name) private channelService: ChannelService,
        @inject(Helper.name) private helper: Helper,
    ) { }


    @ApiOperationGet({
        summary: 'Get all channels.',
        description: 'Get all Channels of database.',
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/')
    getAll(req: express.Request, res: express.Response) {
        return this.channelService.getAll().then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationGet({
        path: '/{id}',
        summary: 'Get unique channel from id.',
        description: 'Get unique channel from id given in params.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/:id')
    get(@requestParam('id') id: string, req: express.Request, res: express.Response) {
        if (this.helper.errorWithParams(req, res))
            return res

        return this.channelService.get(id).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationPost({
        summary: 'Create a new channel.',
        description: 'Create a new channel in database.',
        parameters: {
            body: {
                type: SwaggerDefinitionConstant.OBJECT,
                required: true,
                model: 'ChannelBody',
            },
        },
        responses: {
            201: { description: 'Created' },
            400: { description: 'Parameters fail' },
        },
    })
    @httpPost('/create')
    create(req: express.Request, res: express.Response) {

        if (this.helper.errorWithParams(req, res))
            return res

        return this.channelService.create().then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationDelete({
        path: '/{id}',
        summary: 'Delete a channel.',
        description: 'Delete a channel in database.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpDelete('/delete/:id')
    delete(@requestParam("id") id: string, req: express.Request, res: express.Response) {

        if (this.helper.errorWithParams(req, res))
            return res

        return this.channelService.delete(id).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationPut({
        path: '/{id}',
        summary: 'Add a user to a channel.',
        description: 'Add a user to a channel in database.',
        parameters: {
            body: {
                type: SwaggerDefinitionConstant.OBJECT,
                model: 'ChannelUserBody',
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpPut('/addUser')
    addUser(req: express.Request, res: express.Response) {
        const channel_user = new ChannelUserBody(req.body.channel_id, req.body.user_id)

        return this.channelService.addUser(channel_user).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationPut({
        path: '/{id}',
        summary: 'Remove a user to a channel.',
        description: 'Remove a user to a channel in database.',
        parameters: {
            body: {
                type: SwaggerDefinitionConstant.OBJECT,
                model: 'ChannelUserBody',
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpPut('/removeUser')
    removeUser(req: express.Request, res: express.Response) {
        const channel_user = new ChannelUserBody(req.body.channel_id, req.body.user_id)

        return this.channelService.removeUser(channel_user).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationGet({
        path: '/{id}',
        summary: 'Get all channels from id user.',
        description: 'Get all channels from id user given in params.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/users:id')
    getChannelsByUser(@requestParam("id") id: string, req: express.Request, res: express.Response) {
        return this.channelService.getByUser(id).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })
    }

    @ApiOperationGet({
        path: '/{id_one}/{id_two}',
        summary: 'Get a unique channel from both id users.',
        description: 'Get a unique channel from both id user given in params.',
        parameters: {
            path: {
                id: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpGet('/user/:id_one/:id_two')
    getChannelByUsers(@requestParam("id_one") idOne: string, @requestParam("id_two") idTwo: string, @request() req: express.Request, @response() res: express.Response) {

        return this.channelService.getByUsers(idOne, idTwo).then(result => {
            res.status(OK).json(result)
        }).catch(err => {
            this.helper.handleError(req, res, err)
        })

    }

}