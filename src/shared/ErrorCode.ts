import { BAD_REQUEST, INTERNAL_SERVER_ERROR, NOT_FOUND } from 'http-status-codes'

export interface IErrorCode {
    status: number
    message: string
    items?: any[]
}

export class ErrorCode {

    public static readonly INTERNAL_ERROR: IErrorCode = {
        status: INTERNAL_SERVER_ERROR,
        message: 'Error when asking server.'
    }

    public static readonly BAD_FORMAT_TOKEN: IErrorCode = {
        status: BAD_REQUEST,
        message: 'Bad tokken.'
    }

    public static readonly NOT_FOUND: IErrorCode = {
        status: NOT_FOUND,
        message: 'No ressources found.'
    }

    public static readonly BAD_REQUEST: IErrorCode = {
        status: INTERNAL_SERVER_ERROR,
        message: 'Wrong body.'
    }

    public static readonly NO_CHANNEL: IErrorCode = {
        status: BAD_REQUEST,
        message: 'No channels has been found for this id.'
    }

    public static readonly NO_USER: IErrorCode = {
        status: BAD_REQUEST,
        message: 'No users has been found for this id.'
    }

    public static readonly NO_MESSAGE: IErrorCode = {
        status: BAD_REQUEST,
        message: 'No messages has been found for this id.',
    }

    public static readonly INVALID_UUDID: IErrorCode = {
        status: BAD_REQUEST,
        message: 'Id id not a valid UUIDV4.',
    }

}