import UUIDValide from 'uuid-validate'
import { ChannelService } from './ChannelService'
import { UserService } from './UserService'
import { MessageDto } from '@dtos'
import { MessageModel } from '@models'
import { MessageMapper } from '@mappers'
import { MessageBody } from '@requestbody'
import { injectable, inject } from 'inversify'
import { ErrorCode, logger } from '@shared'

@injectable()
export class MessageService {

    constructor(
        @inject(ChannelService.name) private channelService: ChannelService,
        @inject(UserService.name) private userService: UserService,
    ) { }

    /**
     * Return all messages.
     * 
     * @returns Promise.
     */
    public getAll() {
        return new Promise<MessageDto[]>((resolve, reject) => {

            MessageModel.findAll().then(messages => {
                const messagesDto: MessageDto[] = messages.map(message => MessageMapper.mapToDto(message))
                resolve(messagesDto)
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Return a unique message.
     * 
     * @param message_id (string)
     * @return Promise.
     */
    public get(message_id: string) {
        return new Promise<MessageDto>((resolve, reject) => {

            if (!UUIDValide(message_id)) reject(ErrorCode.INVALID_UUDID)

            MessageModel.findByPk(message_id).then(message => {
                if (!message)
                    reject(ErrorCode.NO_MESSAGE)
                resolve(MessageMapper.mapToDto(message!!))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Create a message in channel.
     * 
     * @param message Body with user and channel data.
     * @return Promise.
     */
    public create(message: MessageBody) {
        return new Promise<MessageDto>((resolve, reject) => {

            this.channelService.exists(message.channel_id).then(isChannelExists => {
                if (!isChannelExists) reject(ErrorCode.NO_CHANNEL)
            })

            this.userService.exists(message.user_id).then(isUserExists => {
                if (!isUserExists) reject(ErrorCode.NO_USER)
            })

            MessageModel.create({
                userID: message.user_id,
                channelID: message.channel_id,
                text: message.text
            }).then(message => {
                resolve(MessageMapper.mapToDto(message))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Delete a message.
     * 
     * @param message_id (string)
     * @returns Promise.
     */
    public delete(message_id: string) {
        return new Promise<string>((resolve, reject) => {

            if (!UUIDValide(message_id))
                reject(ErrorCode.INVALID_UUDID)

            MessageModel.destroy({ where: { id: message_id } }).then(row => {
                resolve(`Line removed : ${row}, for id : ${message_id}`)
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

}