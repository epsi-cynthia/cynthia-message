import { validationResult } from 'express-validator'
import { ErrorCode, IErrorCode, logger } from '@shared'
import { BAD_REQUEST } from 'http-status-codes'
import * as express from 'express'
import { injectable } from 'inversify'

@injectable()
export class Helper {

    public errorWithParams(req: express.Request, res: express.Response): boolean {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error: IErrorCode = ErrorCode.BAD_REQUEST;
            error.items = errors.array();

            this.handleError(req, res, error);
            return true;
        }
        return false;
    }

    public handleError(req: express.Request, res: express.Response, err: IErrorCode): express.Response {
        logger.error(`${BAD_REQUEST} - ${err.message} - ${req.method} - ${req.originalUrl}`);

        return res.status(err.status).json(err);
    }

}