import { UserService } from './UserService'
import { ChannelService } from './ChannelService'
import { MessageService } from './MessageService'
import UUIDV4 from 'uuid-validate'
import { MessageDto } from '@dtos'
import { MessageBody, ChannelUserBody } from '@requestbody'
import { injectable, inject } from 'inversify'
import { ErrorCode, logger } from '@shared'

@injectable()
export class MainService {

    constructor(
        @inject(ChannelService.name) private followerService: ChannelService,
        @inject(UserService.name) private userService: UserService,
        @inject(MessageService.name) private messageService: MessageService
    ) { }

    /**
     * Create a message between two user.
     * If a channel between these two users doesn't exists, then we juste create on channel with these two users.
     * Else we just add the message to the channel.
     * 
     * @param sender_id Id of the messager.
     * @param holder_id Id of the receiver.
     * @param text Message of the messager.
     * @returns Promise.
     */
    public postMessage(sender_id: string, holder_id: string, text: string) {
        return new Promise<MessageDto>((resolve, reject) => {

            if (!UUIDV4(sender_id)) reject(ErrorCode.INVALID_UUDID)

            if (!UUIDV4(holder_id)) reject(ErrorCode.INVALID_UUDID)

            this.userService.exists(sender_id).then(result => {
                if (!result) reject(ErrorCode.NO_USER)
            }).catch(err => reject(err))

            this.userService.exists(holder_id).then(result => {
                if (!result) reject(ErrorCode.NO_USER)
            }).catch(err => reject(err))

            this.followerService.getByUsers(sender_id, holder_id).then(channelDto => {
                if (channelDto) {
                    const newMessage = new MessageBody(sender_id, channelDto.id, text)
                    this.messageService.create(newMessage).then(message => {
                        resolve(message)
                    }).catch(err => {
                        logger.error(err.stack)
                        reject(ErrorCode.INTERNAL_ERROR)
                    })
                } else {
                    this.followerService.create().then(channel => {

                        const channelSenderBody = new ChannelUserBody(channel.id, sender_id)
                        const channelHolderBody = new ChannelUserBody(channel.id, holder_id)
                        this.followerService.addUser(channelSenderBody).catch(err => {
                            logger.error(err.stack)
                            reject(ErrorCode.INTERNAL_ERROR)
                        })
                        this.followerService.addUser(channelHolderBody).catch(err => {
                            logger.error(err.stack)
                            reject(ErrorCode.INTERNAL_ERROR)
                        })
                        const newMessage = new MessageBody(sender_id, channel.id, text)
                        this.messageService.create(newMessage).then(message => {
                            resolve(message)
                        }).catch(err => {
                            logger.error(err.stack)
                            reject(ErrorCode.INTERNAL_ERROR)
                        })

                    })
                }
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

}