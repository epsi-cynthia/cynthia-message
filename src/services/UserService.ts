import UUIDValide from 'uuid-validate'
import { UserDto } from '@dtos'
import { UserModel, ChannelModel, MessageModel } from '@models'
import { UserMapper } from '@mappers'
import { UserBody } from '@requestbody'
import { injectable } from 'inversify'
import { ErrorCode, logger } from '@shared'

@injectable()
export class UserService {

    /**
     * Return all users.
     * 
     * @returns Promise.
     */
    public getAll() {
        return new Promise<UserDto[]>((resolve, reject) => {

            UserModel.findAll({
                include: [ChannelModel, MessageModel]
            }).then(users => {
                const usersDto: UserDto[] = users.map(user => UserMapper.mapToDto(user))
                resolve(usersDto)
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })
        })

    }

    /**
     * Return a unique user.
     * 
     * @param id_user (string)
     * @return Promise.
     */
    public get(id_user: string) {
        return new Promise<UserDto>((resolve, reject) => {

            if (!id_user)
                reject(ErrorCode.BAD_REQUEST)
            if (!UUIDValide(id_user))
                reject(ErrorCode.INVALID_UUDID)

            UserModel.findByPk(id_user, {
                include: [ChannelModel, MessageModel]
            }).then(user => {
                if (!user) reject(ErrorCode.NO_USER)
                resolve(UserMapper.mapToDto(user!!))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })
        })
    }

    /**
     * Create a user.
     * 
     * @param message Body with user.
     * @return Promise.
     */
    public create(user: UserBody) {
        return new Promise<UserDto>((resolve, reject) => {

            if (!UUIDValide(user.id))
                reject(ErrorCode.INVALID_UUDID)

            this.exists(user.id).then(result => {
                if (result) reject({ error: `User id : ${user.id} already exists` })
            })

            UserModel.create({ id: user.id }).then(user => {
                resolve(UserMapper.mapToDto(user))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Delete a user.
     * 
     * @param message Body with user.
     * @return Promise.
     */
    public delete(id_user: string) {
        return new Promise<string>((resolve, reject) => {

            if (!id_user)
                reject(ErrorCode.NO_USER)
            if (!UUIDValide(id_user))
                reject(ErrorCode.INVALID_UUDID)

            UserModel.destroy({ where: { id: id_user } }).then(row => {
                resolve(`Line removed : ${row}, for id : ${id_user}`)
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Check if the user exists.
     * 
     * @param id_channel userId (string).
     * @returns true|false.
     */
    public exists(id_user: string) {
        return new Promise<boolean>((resolve, reject) => {

            if (!id_user)
                reject(ErrorCode.BAD_REQUEST)
            if (!UUIDValide(id_user!!))
                reject(ErrorCode.INVALID_UUDID)

            UserModel.findByPk(id_user!!).then(user => {
                if (!user) resolve(false)
                resolve(true)
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

}