import UUIDValide from 'uuid-validate'
import { UserService } from './UserService'
import { ChannelDto } from '@dtos'
import { ChannelModel, MessageModel, UserModel, ChannelUserModel } from '@models'
import { ChannelMapper } from '@mappers'
import { ChannelUserBody } from '@requestbody'
import { injectable, inject } from 'inversify'
import { logger, ErrorCode } from '@shared'

@injectable()
export class ChannelService {

    constructor(
        @inject(UserService.name) private userService: UserService,
    ) { }

    /**
     * Return all channels from database.
     * 
     * @returns Promise.
     */
    public getAll() {
        return new Promise<ChannelDto[]>((resolve, reject) => {

            ChannelModel.findAll({
                include: [MessageModel, UserModel]
            }).then(channels => {
                resolve(channels.map(channel => ChannelMapper.mapToDto(channel)))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Return a unique channel where id is equals to param id.
     * 
     * @param id_channel Channel id (string)
     * @return Promise
     */
    public get(id_channel: string) {
        return new Promise<ChannelDto>((resolve, reject) => {

            if (!UUIDValide(id_channel))
                reject(ErrorCode.INVALID_UUDID)

            ChannelModel.findByPk(id_channel, {
                include: [MessageModel, UserModel]
            }).then(channel => {
                if (!channel) reject(ErrorCode.NO_CHANNEL)
                resolve(ChannelMapper.mapToDto(channel!!))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Create a channel.
     * 
     * @return Promise.
     */
    public create() {
        return new Promise<ChannelDto>((resolve, reject) => {

            ChannelModel.create().then(channel => {
                resolve(ChannelMapper.mapToDto(channel))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Delete a channel where id is equal to id parameter.
     * @param id_channel ChannelId (string).
     * 
     * @returns Promise.
     */
    public delete(id_channel: string) {
        return new Promise<string>((resolve, reject) => {

            if (!UUIDValide(id_channel))
                reject(ErrorCode.INVALID_UUDID)

            ChannelModel.destroy({ where: { id: id_channel } }).then(row => {
                resolve(`Line(s) removed : ${row}, for id : ${id_channel}`)
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Check if the channel exists.
     * 
     * @param id_channel ChannelId (string).
     * @returns true|false.
     */
    public exists(id_channel: string) {
        return new Promise<boolean>((resolve, reject) => {

            if (!UUIDValide(id_channel))
                reject(ErrorCode.INVALID_UUDID)

            ChannelModel.findByPk(id_channel).then(user => {
                if (!user) resolve(false)
                resolve(true)
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Add user to the channel selected.
     * 
     * @param channel_user Body with user and channel data.
     * @returns Promise.
     */
    public addUser(channel_user: ChannelUserBody) {
        return new Promise((resolve, reject) => {

            this.exists(channel_user.channel_id).then(isChannelExists => {
                if (!isChannelExists) reject(ErrorCode.NO_CHANNEL)
            })

            this.userService.exists(channel_user.user_id).then(isUserExists => {
                if (!isUserExists) reject(ErrorCode.NO_USER)
            })

            ChannelUserModel.create({ channelID: channel_user.channel_id, userID: channel_user.user_id }).then(channelUser => {
                resolve({ success: `No error while inserting : ${channelUser.userID} in ${channelUser.channelID}` })
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Remove user form the channel selected.
     * 
     * @param channel_user Body with user and channel data.
     * @returns Promise.
     */
    public removeUser(channel_user: ChannelUserBody) {
        return new Promise((resolve, reject) => {

            this.exists(channel_user.channel_id).then(isChannelExists => {
                if (!isChannelExists) reject(ErrorCode.NO_CHANNEL)
            })

            this.userService.exists(channel_user.user_id).then(isUserExists => {
                if (!isUserExists) reject(ErrorCode.NO_USER)
            })

            ChannelUserModel.destroy({ where: { channelID: channel_user.channel_id, userID: channel_user.user_id } }).then(row => {
                resolve({ success: `Line(s) removed : ${row} for channel id : ${channel_user.channel_id}` })
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Return channels of user selected.
     * 
     * @param user_id Id of User (string).
     * @returns Promise.
     */
    public getByUser(user_id: string) {
        return new Promise<ChannelDto[]>((resolve, reject) => {

            this.userService.exists(user_id).then(result => {
                if (!result) reject(ErrorCode.NO_USER)
            })

            ChannelModel.findAll(
                { include: [UserModel, MessageModel], }
            ).then(channels => {

                const channelsUser = channels.filter(channel => {
                    return channel.users!!.filter(user => {
                        return user.id === user_id
                    }).length > 0
                })
                resolve(channelsUser.map(channel => ChannelMapper.mapToDto(channel)))

            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

    /**
     * Return channel of the two users selected.
     * 
     * @param users_id_one Id of first user (string).
     * @param user_id_two Id of second user (string).
     * @returns Promise.
     */
    public getByUsers(users_id_one: string, user_id_two: string) {
        return new Promise<ChannelDto>((resolve, reject) => {

            this.userService.exists(users_id_one).then(result => {
                if (!result) reject(ErrorCode.NO_USER)
            })

            this.userService.exists(user_id_two).then(result => {
                if (!result) reject(ErrorCode.NO_USER)
            })

            ChannelModel.findAll({
                include: [UserModel, MessageModel]
            }).then(channels => {

                const channelUser = channels.filter(channel => {
                    if (channel.users) return channel.users.length === 2
                }).filter(channel => {
                    return channel.users!!.filter(user => {
                        return user.id === users_id_one || user.id === user_id_two
                    }).length === 2
                })[0]

                resolve(ChannelMapper.mapToDto(channelUser))
            }).catch(err => {
                logger.error(err.stack)
                reject(ErrorCode.INTERNAL_ERROR)
            })

        })
    }

}