import "reflect-metadata"
import socketIo from 'socket.io'
import { SequelizeConnection } from './SequelizeConnection'
import { logger } from '@shared'

import server from '@server'
import container from '@container'
import { MainService } from '@services'

(async () => {

    await SequelizeConnection.getInstance().sync()

    const io = socketIo(server)

    const port = Number(process.env.PORT || 3000)
    const app = server.build()

    app.listen(port, () => {

        logger.info('Express server started on port: ' + port)

        const mainService = container.get<MainService>(MainService.name)

        io.on('connection', socket => {
            socket.on('message', data => {

                console.log(data)

                mainService.postMessage(data.sender_id, data.holder_id, data.text).then( () => {
                    socket.emit('message_response', { result: `Message send from ${data.sender_id} for ${data.holder_id}` })
                }).catch(err => {
                    socket.emit('message_response', { result: `Error when adding message` })
                })
                
            })
        })

    })
})()
