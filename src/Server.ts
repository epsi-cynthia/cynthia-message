import express from 'express'
import { InversifyExpressServer } from "inversify-express-utils"


import * as swagger from 'swagger-express-ts'
import swaggerUi from 'swagger-ui-express'
import { SwaggerService } from 'swagger-express-ts/swagger.service'

import container from '@container'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import session from 'express-session'
import { KeycloakAuth, ErrorCode } from '@shared'
import { Helper } from '@services'

const server = new InversifyExpressServer(container)

server.setConfig(app => {
    // Add middleware/settings/routes to express.
    app.use(morgan('dev'))
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))
    app.use(cookieParser())

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, DELETE, PUT, PATCH')
        next()
    })

    // Create session
    app.use(session({
        secret: process.env.KC_SESSION_SECRET_KEY || '',
        resave: false,
        saveUninitialized: true,
        store: new session.MemoryStore()
    }));

    // Keycloak authentication
    app.use(KeycloakAuth.getInstance().middleware({
        logout: '/logout',
    }));

    app.use(swagger.express({
        definition: {
            info: {
                title: 'Message Service',
                version: '1.0',
            },
        },
    }))

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(
        SwaggerService.getInstance().getData(),
    ))

})

server.setErrorConfig(app => {

    app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        container.get<Helper>(Helper.name).handleError(req, res, ErrorCode.NOT_FOUND)
    })

    app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
        const errorCode = err.message.includes('keycloak-token')
            ? ErrorCode.BAD_FORMAT_TOKEN
            : ErrorCode.INTERNAL_ERROR

        container.get<Helper>(Helper.name).handleError(req, res, errorCode)
    })

})

export default server
