import { Container } from 'inversify'
import { UserService, MainService, MessageService, ChannelService, Helper } from '@services'

// declare metadata by @controller annotation
import "./controllers/ChannelController"
import "./controllers/MainController"
import "./controllers/MessageController"
import "./controllers/UserController"

const container = new Container()

// Logic services
container.bind<ChannelService>(ChannelService.name).to(ChannelService).inSingletonScope()
container.bind<MessageService>(MessageService.name).to(MessageService).inSingletonScope()
container.bind<UserService>(UserService.name).to(UserService).inSingletonScope()
container.bind<MainService>(MainService.name).to(MainService).inSingletonScope()

// Helper services (error management)
container.bind<Helper>(Helper.name).to(Helper).inSingletonScope()

export default container