export class MessageBody {
    public user_id: string
    public channel_id: string
    public text: string

    constructor(user_id: string, channel_id: string ,text: string) {
        this.user_id = user_id
        this.channel_id = channel_id
        this.text = text
    }
}