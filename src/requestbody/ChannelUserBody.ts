export class ChannelUserBody {
    public channel_id: string
    public user_id: string

    constructor(channel_id: string, user_id: string) {
        this.channel_id = channel_id
        this.user_id = user_id
    }
}