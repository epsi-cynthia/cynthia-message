import { Mapper } from '@wufe/mapper'
import { ChannelModel } from '@models'
import { ChannelDto } from '@dtos'

export abstract class ChannelMapper {

    private static mapper: Mapper

    public static mapToDto(message: ChannelModel): ChannelDto {
        return this.initializeMapper().map<ChannelModel, ChannelDto>(message)
    }

    private static initializeMapper(): Mapper {
        if (!this.mapper) {
            this.mapper = new Mapper()
                .withConfiguration((configuration) => configuration.shouldIgnoreSourcePropertiesIfNotInDestination(true))

            this.mapper.createMap<ChannelModel, ChannelDto>(ChannelModel)
                .forMember('id', opt => opt.mapFrom(src => src.id))
                .forMember('users', opt => opt.mapFrom(src => src.users))
                .forMember('messages', opt => opt.mapFrom(src => src.messages))
        }

        return this.mapper
    }

}