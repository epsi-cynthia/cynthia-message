import {Mapper} from '@wufe/mapper'
import { MessageModel } from '@models'
import { MessageDto } from '@dtos'

export abstract class MessageMapper {

    private static mapper: Mapper

    public static mapToDto(message: MessageModel): MessageDto {
        return this.initializeMapper().map<MessageModel, MessageDto>(message)
    }

    private static initializeMapper(): Mapper {
        if (!this.mapper) {
            this.mapper = new Mapper()
                .withConfiguration((configuration) => configuration.shouldIgnoreSourcePropertiesIfNotInDestination(true))

            this.mapper.createMap<MessageModel, MessageDto>(MessageModel)
                .forMember('id', opt => opt.mapFrom(src => src.id))
                .forMember('userID', opt => opt.mapFrom(src => src.userID))
                .forMember('text', opt => opt.mapFrom(src => src.text))
                .forMember('channelID', opt => opt.mapFrom(src => src.channelID))
        }

        return this.mapper
    }

}