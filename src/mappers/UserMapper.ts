import { Mapper } from '@wufe/mapper'
import { UserModel } from '@models'
import { UserDto } from '@dtos'


export abstract class UserMapper {

    private static mapper: Mapper

    public static mapToDto(message: UserModel): UserDto {
        return this.initializeMapper().map<UserModel, UserDto>(message)
    }

    private static initializeMapper(): Mapper {
        if (!this.mapper) {
            this.mapper = new Mapper()
                .withConfiguration((configuration) => configuration.shouldIgnoreSourcePropertiesIfNotInDestination(true))

            this.mapper.createMap<UserModel, UserDto>(UserModel)
                .forMember('id', opt => opt.mapFrom(src => src.id))
                .forMember('channels', opt => opt.mapFrom(src => src.channels))
                .forMember('messages', opt => opt.mapFrom(src => src.messages))
        }

        return this.mapper
    }

}