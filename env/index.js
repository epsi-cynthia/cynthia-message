const dotenv = require('dotenv');

// Set default to "development"
const nodeEnv = process.env.ENV_FILE || 'development';
const path = `./env/${nodeEnv}.env`;

const result = dotenv.config({ path });

if (result.error) {
    console.log(result.error.message)
}