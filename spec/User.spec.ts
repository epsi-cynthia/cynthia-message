import { SuperTest, Test } from 'supertest'
import { UserDto } from '@dtos'
import { SequelizeConnection } from 'src/SequelizeConnection'
import { UserModel } from '@models'
import supertest = require('supertest')
import server from '@server'
import { OK, CREATED } from 'http-status-codes'

describe('User', () => {

    let agent: SuperTest<Test>

    const user_one = new UserDto(
        '6b2f5e2a-6b0e-4129-af19-8c82b1a245dd',
        [],
        []
    )

    const user_two = new UserDto(
        '8b6c8b3a-6f60-4d23-98d2-f7a08f3571bf',
        [],
        []
    )

    it('test_calcul', () => {
        expect(20 + 20 ).toBe(40)
    })

    /**
    
    beforeAll(async doneFn => {

        await SequelizeConnection.getInstance().sync()

        UserModel.destroy({ where: {} })
            .then(() => {

                const userModel = new UserModel(user_one)

                userModel.save().then(user => {
                    user_one.createdAt = user.createdAt
                    user_one.updatedAt = user.updatedAt

                    agent = supertest.agent(server.build())
                    doneFn()
                }).catch(console.log)
            }).catch(console.log)
    })

    it('GetAllUsers', doneFn => {
        return agent.get('/users')
            .expect(OK)
            .end((err, res) => {
                expect(res.body.lenght).toBeGreaterThan(0)
                doneFn()
            })
    })

    it('GetOneUserByID', doneFn => {
        return agent.get(`/users/${user_one.id}`)
            .expect(OK)
            .end((err, res) => {
                expect(res.body.id).toBe(user_one.id)
                doneFn()
            })
    })

    it('CreateUser', doneFn => {
        return agent.post('/users/create')
            .type('json')
            .send({
                id: user_two.id,
                channels: user_two.channels,
                messages: user_two.messages
            })
            .expect(CREATED)
            .end((err, res) => {
                if (res.header.location) {
                    const location = String(res.header.location).split('/')
                    user_two.id = location[location.length - 1]
                }
                doneFn()
            })
    })

    it('DeleteUser', () => {
        return agent.delete(`/users/${user_one.id}`)
            .expect(OK)
    })

     */

})
